import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import {reducers} from "./store";
import * as firebase from "./api/firebase";
// import firebaseSchema from "./api/firebase/schema";
// import configureFirebase from "./api/firebase/configure";

const store = createStore(reducers, applyMiddleware(thunk));

const connectedApp = (
    <Provider store={store}>
        <App />
    </Provider>
)

ReactDOM.render(connectedApp, document.getElementById('root'));
// configureFirebase(firebaseSchema);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
