const proxy = require('http-proxy-middleware');

//setup proxy for firebase
module.exports = function(app) {
  app.use(proxy('/__', { target: 'http://localhost:5000/' }));
};