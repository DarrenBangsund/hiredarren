export default function about(state = {}, action) {
    switch (action.type) {
        case 'FETCH_ABOUT':
            return Object.assign({}, state, action.payload);
        default: 
            return state;
    }
}