const configure = (database, schema, path = "") => {
    const nextObj = {};

    if(typeof schema == "object" && !Array.isArray(schema)) {
        const k = Object.keys(schema);
        
        k.forEach(element => {
            nextObj[element] = configure(database, schema[element], `${path}/${element}`);
        });

        return nextObj;
    } else {
        console.log(`CREATING${Array.isArray(schema) ? " ARRAY" : ""}: `, path);
        return (page = 0, count = 50) => new Resource(database, path, Array.isArray(schema), page, count);
    }
}

const withId = (path, id) => {
    return `${path}/${id}`;
}

class Resource {
    constructor(database, path, paginatable, page, count) {
        this.path = path;
        this.database = database;
        
        if (paginatable) {
            this.start = page*count;
            this.end = (page*count)+(count-1);
            
            this.fetchMany = () => this.database().ref(this.path).orderByKey().startAt(this.start.toString()).endAt(this.end.toString()).once("value");
            this.fetchById = (id) => this.database().ref(withId(this.path, id)).once("value");
            this.patchById = (id, value) => this.database().ref(this.path).child(id).set(value);
            this.subscribe = (cb) => this.database().ref(this.path).on("value", cb);
        } else {
            this.start = 0;
            this.end = 0;

            this.fetch = () => this.database().ref(this.path).once("value");
            this.patch = (value) => this.database().ref(this.path).set(value);
        }
    }
    
}

export const configureDb = (database, schema) => {
    const db = configure(database, schema);
    console.log(db);
    db.about.fetch(snapshot => {
        console.log(snapshot.toJSON());
    })
}

// export default configure;