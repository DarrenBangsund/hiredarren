const types = {
    String: "string",
    Link: {
        icon: "String",
        location: "String",
        title: "String",
    },
    Item: {
        title: "String",
        children: []
    }
}

export default {
    about: {
        details: [
            {
                icon: types.String,
                location: types.String,
                title: types.String
            }
        ],
        name: types.String,
        profession: types.String,
    },
    education: [{
        date: types.String,
        title: types.String
    }],
    experience: [{
        date: types.String,
        description: types.String,
        link: types.Link,
        position: types.String,
        tasks: [types.String]
    }],
    skills: [types.Item]
}