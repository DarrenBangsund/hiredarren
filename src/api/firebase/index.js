import * as firebase from "firebase";
import config from "./config.json";
import schema from "./schema";
import {configureDb} from "./configure";

firebase.initializeApp(config);

console.log(configureDb(firebase.database, schema));