const db = firebase.database();

export function get(name) {
    return Promise((resolve, reject) => {
        db.ref(name).once('value')
        .then((data) =>{ resolve(data.toJSON()); })
        .catch(reject);
    });
}

console.log(get("education"))